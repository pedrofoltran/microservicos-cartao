package br.com.itau.cliente.models.DTOs;

public class EntradaPostClienteDTO {
    private String name;

    public EntradaPostClienteDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
