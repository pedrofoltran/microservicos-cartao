package br.com.itau.cliente.repositories;

import br.com.itau.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Optional<Cliente> findByUsuarioId(int id);
}
