package br.com.itau.cliente.models.mappers;

import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.models.DTOs.EntradaPostClienteDTO;
import br.com.itau.cliente.models.Usuario;

public class ClienteMapper {
    public static Cliente entradaParaCliente(EntradaPostClienteDTO entradaPostClienteDTO, Usuario usuario)
    {
        Cliente cliente = new Cliente();
        cliente.setName(entradaPostClienteDTO.getName());
        cliente.setUsuarioId(usuario.getId());
        return cliente;
    }
}
