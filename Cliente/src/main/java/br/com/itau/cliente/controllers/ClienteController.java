package br.com.itau.cliente.controllers;

import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.models.DTOs.EntradaPostClienteDTO;
import br.com.itau.cliente.models.Usuario;
import br.com.itau.cliente.models.mappers.ClienteMapper;
import br.com.itau.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.ws.rs.Path;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente(@AuthenticationPrincipal Usuario usuario,
                                @RequestBody EntradaPostClienteDTO entradaPostClienteDTO){
        return clienteService.criarCliente(ClienteMapper.entradaParaCliente(entradaPostClienteDTO, usuario));
    }

    @GetMapping("/{id}")
    public Cliente buscarClientePorID(@PathVariable int id){
        try {
            return clienteService.buscarClientePorID(id);
        } catch (RuntimeException re){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }

    }

}
