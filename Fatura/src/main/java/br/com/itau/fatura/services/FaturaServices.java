package br.com.itau.fatura.services;

import br.com.itau.fatura.clients.CartaoClient;
import br.com.itau.fatura.models.Cartao;
import br.com.itau.fatura.models.DTOs.AlterarAtivoStatusDTO;
import br.com.itau.fatura.models.DTOs.SaidaPostExpirarFaturaDTO;
import br.com.itau.fatura.repositories.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FaturaServices {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public SaidaPostExpirarFaturaDTO expirarCartao(int clientId, int cartaoId) {
        AlterarAtivoStatusDTO alterarAtivoStatusDTO = new AlterarAtivoStatusDTO();
        alterarAtivoStatusDTO.setAtivo(false);

        Cartao cartao = cartaoClient.getNumeroCartao(cartaoId);

        cartaoClient.alterarStatus(cartao.getNumero(), alterarAtivoStatusDTO);

        SaidaPostExpirarFaturaDTO saidaPostExpirarFaturaDTO = new SaidaPostExpirarFaturaDTO();
        saidaPostExpirarFaturaDTO.setStatus("ok");
        return saidaPostExpirarFaturaDTO;
    }
}
