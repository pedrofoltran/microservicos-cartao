package br.com.itau.fatura.controllers;

import br.com.itau.fatura.models.DTOs.SaidaGetFaturaDTO;
import br.com.itau.fatura.models.DTOs.SaidaPostExpirarFaturaDTO;
import br.com.itau.fatura.services.FaturaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {
    @Autowired
    FaturaServices faturaServices;

    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<SaidaGetFaturaDTO> getFatura(@PathVariable(name = "cliente-id") int clientId,
                                             @PathVariable(name = "cartao-id") int cartaoId) {
        return null;
    }

    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public SaidaPostExpirarFaturaDTO postExpirar(@PathVariable(name = "cliente-id") int clientId,
                                                 @PathVariable(name = "cartao-id") int cartaoId){

        return faturaServices.expirarCartao(clientId, cartaoId);

    }
}
