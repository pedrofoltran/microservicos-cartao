package br.com.itau.fatura.models.DTOs;

public class AlterarAtivoStatusDTO {
    public boolean ativo;

    public AlterarAtivoStatusDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
