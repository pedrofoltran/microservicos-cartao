package br.com.itau.fatura.models.DTOs;

import java.math.BigDecimal;

public class SaidaGetFaturaDTO {
    private int id;
    private int cartao_id;
    private String descricacao;
    private BigDecimal valor;

    public SaidaGetFaturaDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricacao() {
        return descricacao;
    }

    public void setDescricacao(String descricacao) {
        this.descricacao = descricacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
