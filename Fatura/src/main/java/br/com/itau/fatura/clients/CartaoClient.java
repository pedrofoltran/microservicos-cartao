package br.com.itau.fatura.clients;


import br.com.itau.fatura.models.Cartao;
import br.com.itau.fatura.models.DTOs.AlterarAtivoStatusDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "cartao")
public interface CartaoClient {

    @PatchMapping("cartao/{numero}")
    Cartao alterarStatus(@PathVariable String numero, @RequestBody AlterarAtivoStatusDTO alterarAtivoStatusDTO);

    @GetMapping("cartao/id/{id}")
    Cartao getNumeroCartao(@PathVariable int id);

}
