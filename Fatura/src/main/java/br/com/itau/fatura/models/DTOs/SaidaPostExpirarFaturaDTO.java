package br.com.itau.fatura.models.DTOs;

public class SaidaPostExpirarFaturaDTO {
    private String status;

    public SaidaPostExpirarFaturaDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
