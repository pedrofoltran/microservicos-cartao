package br.com.itau.pagamento.clients;

import br.com.itau.pagamento.exceptions.CartaoOfflineException;
import br.com.itau.pagamento.models.Cartao;

public class CartaoClientFallback implements CartaoClient{

    @Override
    public Cartao buscarCartao(String numero) {
        throw new CartaoOfflineException();
    }

    @Override
    public Cartao buscarCartaoPorID(int id) {
        throw new CartaoOfflineException();
    }
}
