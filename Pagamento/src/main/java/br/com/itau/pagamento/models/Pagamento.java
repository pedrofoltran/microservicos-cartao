package br.com.itau.pagamento.models;

import javax.persistence.*;

@Entity
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String descricao;
    private double valor;
    private int cartaoid;

    public Pagamento() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getCartaoid() {
        return cartaoid;
    }

    public void setCartaoid(int cartaoid) {
        this.cartaoid = cartaoid;
    }
}
