package br.com.itau.pagamento.services;


import br.com.itau.pagamento.clients.CartaoClient;
import br.com.itau.pagamento.models.Cartao;
import br.com.itau.pagamento.models.DTOs.PagamentoEntradaDTO;
import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Pagamento pagarItem(PagamentoEntradaDTO pagamentoEntradaDTO) {
        Pagamento pagamento = new Pagamento();
        Cartao cartao = cartaoClient.buscarCartaoPorID(pagamentoEntradaDTO.getCartao_id());

        pagamento.setCartaoid(cartao.getId());
        pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
        pagamento.setValor(pagamentoEntradaDTO.getValor());

        return pagamentoRepository.save(pagamento);

    }

    public List<Pagamento> listarPagamentos(int id) {
        Cartao cartao = cartaoClient.buscarCartaoPorID(id);

        return pagamentoRepository.findAllByCartaoid(cartao.getId());
    }
}
