package br.com.itau.pagamento.clients;

import br.com.itau.pagamento.exceptions.CartaoNaoExisteException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 400) {
            throw new CartaoNaoExisteException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
