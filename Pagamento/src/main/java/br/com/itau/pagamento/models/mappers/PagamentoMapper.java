package br.com.itau.pagamento.models.mappers;

import br.com.itau.pagamento.models.DTOs.PagamentoSaidaDTO;
import br.com.itau.pagamento.models.Pagamento;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PagamentoMapper {
    public static PagamentoSaidaDTO toPagamentoSaidaDTO(Pagamento pagamento){
        return new PagamentoSaidaDTO(pagamento.getId(),
                pagamento.getCartaoid(),
                pagamento.getDescricao(),
                new BigDecimal(pagamento.getValor()).setScale(2, RoundingMode.HALF_UP));
    }
}
