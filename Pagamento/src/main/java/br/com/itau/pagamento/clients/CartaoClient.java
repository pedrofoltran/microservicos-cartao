package br.com.itau.pagamento.clients;

import br.com.itau.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClienteConfiguration.class)
public interface CartaoClient {

    @GetMapping("cartao/{numero}")
    Cartao buscarCartao(@PathVariable String numero);

    @GetMapping("cartao/id/{id}")
    Cartao buscarCartaoPorID(@PathVariable int id);
}
