package br.com.itau.pagamento.repositories;


import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartaoid(int cartaoid);
}
