package br.com.itau.cartao.client;

import br.com.itau.cartao.exceptions.ClienteOfflineException;
import br.com.itau.cartao.models.Cliente;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente buscarClientePorID(int id) {
        throw new ClienteOfflineException();
    }
}
