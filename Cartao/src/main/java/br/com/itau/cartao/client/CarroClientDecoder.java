package br.com.itau.cartao.client;

import br.com.itau.cartao.exceptions.ClienteInvalidoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CarroClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();


    @Override
    public Exception decode(String s, Response response) {
        throw new ClienteInvalidoException();
    }
}
