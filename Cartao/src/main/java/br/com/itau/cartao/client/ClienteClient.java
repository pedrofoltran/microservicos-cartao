package br.com.itau.cartao.client;

import br.com.itau.cartao.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("cliente/{id}")
    Cliente buscarClientePorID(@PathVariable int id);
}
