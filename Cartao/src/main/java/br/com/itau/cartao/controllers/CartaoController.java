package br.com.itau.cartao.controllers;


import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.DTOs.AlterarAtivoEntradaDTO;
import br.com.itau.cartao.models.DTOs.CartaoEntradaDTO;
import br.com.itau.cartao.models.DTOs.CartaoSaidaDTO;
import br.com.itau.cartao.models.DTOs.CartaoSaidaGetDTO;
import br.com.itau.cartao.models.Usuario;
import br.com.itau.cartao.models.mappers.CartaoMapper;
import br.com.itau.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Timestamp;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO criaCartao(@AuthenticationPrincipal Usuario usuario,
                                     @RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO) {
        Cartao cartao = cartaoService.criarCartao(cartaoEntradaDTO, usuario);
        System.out.println("[POST] /: " + System.currentTimeMillis());
        return CartaoMapper.toCartaoSaidaDTO(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoSaidaDTO alterarAtivo(@AuthenticationPrincipal Usuario usuario,
                                       @PathVariable String numero,
                                       @RequestBody AlterarAtivoEntradaDTO alterarAtivoEntradaDTO) {
        Cartao cartao = cartaoService.alterarStatus(numero, alterarAtivoEntradaDTO);
        System.out.println("[PATCH] /{numero}: " + System.currentTimeMillis());
        return CartaoMapper.toCartaoSaidaDTO(cartao);

    }

    @GetMapping("/{numero}")
    public CartaoSaidaGetDTO buscarCartao(@PathVariable String numero) {
        Cartao cartao = cartaoService.buscarCartao(numero);
        System.out.println("[GET] /{numero}: " + System.currentTimeMillis());
        return CartaoMapper.toCartaoSaidaGetDTO(cartao);
    }

    @GetMapping("/id/{id}")
    public CartaoSaidaGetDTO buscarCartao(@PathVariable int id) {
        Cartao cartao = cartaoService.buscarCartao(id);
        System.out.println("[GET] /{id}: " + System.currentTimeMillis());
        return CartaoMapper.toCartaoSaidaGetDTO(cartao);
    }
}
