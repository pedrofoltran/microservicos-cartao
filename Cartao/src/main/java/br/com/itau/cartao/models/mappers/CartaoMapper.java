package br.com.itau.cartao.models.mappers;

import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.DTOs.CartaoSaidaDTO;
import br.com.itau.cartao.models.DTOs.CartaoSaidaGetDTO;

public class CartaoMapper {
    public static CartaoSaidaDTO toCartaoSaidaDTO(Cartao cartao) {
        return new CartaoSaidaDTO(cartao.getId(),
                cartao.getNumero(),
                cartao.getClienteId(),
                cartao.getAtivo());
    }

    public static CartaoSaidaGetDTO toCartaoSaidaGetDTO(Cartao cartao) {
        return new CartaoSaidaGetDTO(cartao.getId(),
                cartao.getNumero(),
                cartao.getClienteId());
    }

}
