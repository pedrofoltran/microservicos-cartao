package br.com.itau.cartao.services;

import br.com.itau.cartao.client.ClienteClient;
import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.Cliente;
import br.com.itau.cartao.models.DTOs.AlterarAtivoEntradaDTO;
import br.com.itau.cartao.models.DTOs.CartaoEntradaDTO;
import br.com.itau.cartao.models.Usuario;
import br.com.itau.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;
    @Autowired
    ClienteClient clienteClient;

    public Cartao criarCartao(@Valid CartaoEntradaDTO cartaoEntradaDTO, Usuario usuario) {
        Cartao cartao = new Cartao();
        Cliente cliente = clienteClient.buscarClientePorID(cartaoEntradaDTO.getClienteId());

        cartao.setNumero(cartaoEntradaDTO.getNumero());
        cartao.setClienteId(cliente.getId());
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao alterarStatus(String numero, AlterarAtivoEntradaDTO alterarAtivoEntradaDTO) {
        Cartao cartao = buscarCartao(numero);
        cartao.setAtivo(alterarAtivoEntradaDTO.isAtivo());
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarCartao(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent())
            return cartaoOptional.get();
        else
            throw new RuntimeException("Cartao nao encontrado");
    }

    public Cartao buscarCartao(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if (cartaoOptional.isPresent())
            return cartaoOptional.get();
        else
            throw new RuntimeException("Cartao nao encontrado");
    }
}
